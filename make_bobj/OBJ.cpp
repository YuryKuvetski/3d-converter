#include "OBJ.h"


OBJ::OBJ(ID3D11Device* pDevice, ID3D11DeviceContext* pImmediateContext, char *fileNameWithoutFormat)
{
	_objFileName = _bobjFileName = _mtlFileName = _txtFileName = fileNameWithoutFormat;
	_objFileName += ".obj";
	_bobjFileName += ".bobj";
	_mtlFileName += ".mtl";
	_txtFileName += ".txt";
	_rot = 0.0f;

	_pDevice = pDevice;
	_pImmediateContext = pImmediateContext;
	_pVertexBuffer = _pIndexBuffer = _pConstantBuffer = nullptr;
	_pPixelShader = nullptr;
	_pVertexShader = nullptr;
	_pSamplerState = nullptr;
	_pLayout = nullptr;
	
	_subsets.push_back(SUBSET());
}
OBJ::~OBJ()
{
	ClearMemory();

	_RELEASE(_pVertexBuffer);
	_RELEASE(_pIndexBuffer);
	_RELEASE(_pConstantBuffer);
	_RELEASE(_pSamplerState);
	_RELEASE(_pVertexShader);
	_RELEASE(_pPixelShader);
	_RELEASE(_pLayout);
	for (auto i = _textures.begin(); i != _textures.end(); i++)
	{
		_RELEASE((*i));
	}
	_textures.clear();
}
void OBJ::ClearMemory()
{
	_vertices.clear();
	_texCoords.clear();
	_clearData.clear();
	_normals.clear();
	_indices.clear();
	_subsets.clear();
}

void OBJ::LoadFromObjFileToMemory()
{
	fstream fin(_objFileName, fstream::in);
	char checkChar;
	VERTEX tmp;

	if (fin)
	{
		while (!fin.eof())
		{
			fin >> checkChar;	// ��� ����� ���������� ������������ �������

			// ����������?
			if (checkChar == 'v')
			{
				checkChar = fin.get();
				float x, y, z;
				fin >> x >> y;
				// �������?
				if (checkChar == 'n')
				{
					 fin >> z;
					_normals.push_back(XMFLOAT3(x, y, z*-1));
				}
				// ���������� ����������?
				else if (checkChar == 't')
				{
					_texCoords.push_back(XMFLOAT2(x, y*-1));
				}
				else // ������ �������
				{
					fin >> z;
					_vertices.push_back(XMFLOAT3(x, y, z*-1));
				}
			}
			// �������?
			else if (checkChar == 'f')
			{
				UINT iPosition, iTexCoord, iNormal;
				VERTEX vertex;

				for (UINT iFace = 0; iFace < 3; iFace++)
				{
					ZeroMemory(&vertex, sizeof(VERTEX));

					// OBJ ������ ���������� ������� � ��������� �� 1
					fin >> iPosition;
					vertex._position = _vertices[iPosition - 1];

					if ('/' == fin.peek())
					{
						fin.ignore();

						if ('/' != fin.peek())
						{
							// �������� ���������� ����������
							fin >> iTexCoord;
							vertex._texCoord = _texCoords[iTexCoord - 1];
						}

						if ('/' == fin.peek())
						{
							fin.ignore();

							// �������� �������
							fin >> iNormal;
							vertex._normal = _normals[iNormal - 1];
						}
					}

					//��������� ������� � ������
					int index = -1;
					for (int i = 0; i < _clearData.size(); i++)
					{
						if (memcmp(&_clearData[i], &vertex, sizeof(VERTEX)) == 0)
						{
							index = i;
							break;
						}
					}
					if (index < 0)
					{
						_clearData.push_back(vertex);
						_indices.push_back(_clearData.size() - 1);
					} 
					else
					{
						_indices.push_back(index);
					}
				}
			}
			// ���� ����������� ���� � �����������
			else if (checkChar == 'm')
			{
				fin.ignore(6);
				fin >> _mtlFileName;
			}
			// ���� ���������� ��������
			else if (checkChar == 'u')
			{
				fin.ignore(6);
				string mtlName;
				fin >> mtlName;
				_subsets.back()._material = GetMaterialFromFile(mtlName);
			}
			// ���� ����� ������
			else if (checkChar == 'g')
			{
				if (_indices.size() != 0)
				{
					_subsets.push_back(SUBSET(_indices.size()));
				}
			}
			if (checkChar != '\n')
			{
				fin.ignore(INT_MAX, '\n');
			}
		}
		fin.close();
		_vertices.clear();
		_normals.clear();
		_texCoords.clear();
	}
}

void OBJ::CreateBobjFromMemory()
{
	fstream binout(_bobjFileName, fstream::out | fstream::trunc | fstream::binary);
	// ����� �������
	int numSubsets = _subsets.size();
	int numVertices = _clearData.size();
	int numIndices = _indices.size();

	binout.write((char*)&numVertices, sizeof(int));
	binout.write((char*)&numIndices, sizeof(int));
	binout.write((char*)&numSubsets, sizeof(int));

	// ����� �������
	for (auto i = _clearData.begin(); i != _clearData.end(); i++)
	{
		binout.write((char*)&(*i), sizeof(VERTEX));
	}

	// ����� �������
	for (auto i = _indices.begin(); i != _indices.end(); i++)
	{
		binout.write((char*)&(*i), sizeof(DWORD));
	}

	// ����� ������
	for (auto i = _subsets.begin(); i != _subsets.end(); i++)
	{
		binout.write((char*)&(*i), sizeof(SUBSET));
	}

	binout.close();
}

void OBJ::CreateTXTFromMemory()
{
	fstream fout(_txtFileName, fstream::out | fstream::trunc);
	// ����� �������
	fout << "SubsetsNum = " << _subsets.size() << '\n' << "VerticesNum = " << _clearData.size() << '\n' << "IndexesNum = " << _indices.size() << endl;

	fout << "\nVERTICES" << endl;
	// ����� �������
	int count = 0;
	for (auto i = _clearData.begin(); i != _clearData.end(); i++, count++)
	{
		fout << "--------- " + to_string(count) +" ---------" << endl;
		fout << "p = " << (*i)._position.x << ' ' << (*i)._position.y << ' ' << (*i)._position.z << endl;
		fout << "t = " << (*i)._texCoord.x << ' ' << (*i)._texCoord.y << endl;
		fout << "n = " << (*i)._normal.x << ' ' << (*i)._normal.y << ' ' << (*i)._normal.z << endl;
		fout << "---------------------" << endl;
		fout << endl;
	}

	fout << "\nINDEXES" << endl;
	// ����� �������
	count = 0;
	for (auto i = _indices.begin(); i != _indices.end(); i++, count++)
	{
		if (count % 3 == 0) { fout << endl; }
		fout << (*i) << ' ';
	}

	fout << "\nSUBSETS" << endl;
	// ����� ������
	for (auto i = _subsets.begin(); i != _subsets.end(); i++)
	{
		fout << "---------------------------START SUBSET---------------------------------" << endl;
		fout << "Pos in indexes = " << (*i)._startIndex << endl;
		fout << "Material info\n" << (*i)._material;
		fout << "----------------------------END SUBSET----------------------------------" << endl;
	}

	fout.close();
}

ostream& operator<<(ostream& os, const MATERIAL& mtl)
{
	os << "AmbientColor = " << mtl._ambientColor.x << ' ' << mtl._ambientColor.y << ' ' << mtl._ambientColor.z << ' ' << mtl._ambientColor.w << endl;
	os << "DiffuseColor = " << mtl._diffuseColor.x << ' ' << mtl._diffuseColor.y << ' ' << mtl._diffuseColor.z << ' ' << mtl._diffuseColor.w << endl;
	os << "SpecularColor = " << mtl._specularColor.x << ' ' << mtl._specularColor.y << ' ' << mtl._specularColor.z << ' ' << mtl._specularColor.w << endl;
	os << "SpecularPower = " << mtl._specularPower << endl;
	os << "IlluminationModel = " << mtl._illuminationModel << endl;
	os << "AmbientTextureMap = " << mtl._ambientTextureMap << endl;
	os << "DiffuseTextureMap = " << mtl._diffuseTextureMap << endl;
	os << "SpecularColorTextureMap = " << mtl._specularColorTextureMap << endl;
	os << "SpecularHighLightComponent = " << mtl._specularHighLightComponent << endl;
	os << "AlphaTextureMap = " << mtl._alphaTextureMap << endl;
	os << "BumpMap = " << mtl._bumpMap << endl;
	os << "DisplacementMap = " << mtl._displacementMap << endl;
	os << "StencilDecalTexture = " << mtl._stencilDecalTexture << endl;
	return os;
}

MATERIAL OBJ::GetMaterialFromFile(string mtlName)
{
	MATERIAL newMaterial;
	ifstream fin(_mtlFileName);
	char checkChar;

	if (fin)
	{
		// ������ � ����� ������ ��� ��������
		string tmpStr = "";
		while (getline(fin, tmpStr))
		{
			if (tmpStr.find(mtlName) != string::npos)
			{
				break;	// ����� ��������� ������
			}
		}

		while (fin)
		{
			fin >> checkChar;	// ��� ����� ���������� ������������ �������

			switch (checkChar)
			{
			case 'K':	// ��� Ka, Kd, Ks
				checkChar = fin.get();
				fin.ignore(1);
				if (checkChar == 'a') { fin >> newMaterial._ambientColor.x >> newMaterial._ambientColor.y >> newMaterial._ambientColor.z; }
				else if (checkChar == 'd') { fin >> newMaterial._diffuseColor.x >> newMaterial._diffuseColor.y >> newMaterial._diffuseColor.z; }
				else if (checkChar == 's') { fin >> newMaterial._specularColor.x >> newMaterial._specularColor.y >> newMaterial._specularColor.z; }
				break;
			case 'N':	// ��� Ns
				fin.ignore(2);
				fin >> newMaterial._specularPower;
				break;
			case 'T':	// ��� Tr
				fin.ignore(2);
				fin >> newMaterial._diffuseColor.w;
				newMaterial._ambientColor.w = newMaterial._specularColor.w = newMaterial._diffuseColor.w;
				break;
			case 'd':	// ��� d, disp, decal
				checkChar = fin.get();
				if (' ' == checkChar) 
				{ 
					fin >> newMaterial._diffuseColor.w;
					newMaterial._diffuseColor.w = 1.0f - newMaterial._diffuseColor.w;
					newMaterial._ambientColor.w = newMaterial._specularColor.w = newMaterial._diffuseColor.w;
				}
				else if ('i' == checkChar) { fin.ignore(3); fin >> newMaterial._displacementMap; }
				else if ('e' == checkChar) { fin.ignore(4); fin >> newMaterial._stencilDecalTexture; }
				break;
			case 'i':	// ��� illum
				fin.ignore(5);
				fin >> newMaterial._illuminationModel;
				break;
			case 'b':	// ��� bump
				fin.ignore(4);
				fin >> newMaterial._bumpMap;
				break;
			case 'm':	// ��� map_Ka, map_Kd, map_Ks, map_Ns, map_d, map_bump
				fin.ignore(3);
				checkChar = fin.get();
				switch (checkChar)
				{
				case 'K':
					checkChar = fin.get();
					fin.ignore(1);
					if ('a' == checkChar) { fin >> newMaterial._ambientTextureMap; }
					else if ('d' == checkChar) { fin >> newMaterial._diffuseTextureMap; }
					else if ('s' == checkChar) { fin >> newMaterial._specularColorTextureMap; }
					break;
				case 'N':
					fin.ignore(2);
					fin >> newMaterial._specularHighLightComponent;
					break;
				case 'd':
					fin.ignore(1);
					fin >> newMaterial._alphaTextureMap;
					break;
				case 'b':
					fin.ignore(4);
					fin >> newMaterial._bumpMap;
					break;
				}
				break;
			default:
				break;
			}
			fin.ignore(INT_MAX, '\n');
		}
		fin.close();
	}
	return newMaterial;
}

void OBJ::LoadFromBobjFileToMemory()
{
	int numSubsets, numVertices, numIndices;
	
	ClearMemory();

	fstream bin(_bobjFileName, fstream::in | fstream::binary);

	if (bin)
	{
		bin.read((char*)&numVertices, sizeof(int));
		bin.read((char*)&numIndices, sizeof(int));
		bin.read((char*)&numSubsets, sizeof(int));

		VERTEX tmpVertex;
		for (int i = 0; i < numVertices; i++)
		{
			bin.read((char*)&tmpVertex, sizeof(VERTEX));
			_clearData.push_back(tmpVertex);
		}

		DWORD tmpIndex;
		for (int i = 0; i < numIndices; i++)
		{
			bin.read((char*)&tmpIndex, sizeof(DWORD));
			_indices.push_back(tmpIndex);
		}

		SUBSET tmpSubset;
		for (int i = 0; i < numSubsets; i++)
		{
			bin.read((char*)&tmpSubset, sizeof(SUBSET));
			_subsets.push_back(tmpSubset);
		}
	}
}

void OBJ::Init(XMMATRIX view, XMMATRIX projection)
{
	_view = view;
	_projection = projection;

	CreateBuffers();

	LoadTextures();

	InitShader();
}

HRESULT OBJ::_compileShaderFromFile(WCHAR* FileName, LPCSTR EntryPoint, LPCSTR ShaderModel, ID3DBlob** ppBlobOut)
{
	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	flags |= D3DCOMPILE_DEBUG;
#endif
	HRESULT hr = S_OK;
	ID3DBlob* pErrorBlob = NULL;
	hr = D3DCompileFromFile(FileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, EntryPoint, ShaderModel, flags, 0, ppBlobOut, &pErrorBlob);
	if (pErrorBlob) { MessageBox(0, L"CompileShaderError", ToString((char*)pErrorBlob->GetBufferPointer()), 0); pErrorBlob->Release(); }
	_RELEASE(pErrorBlob);
	return hr;
}
bool OBJ::InitShader()
{
	ID3DBlob *vertexShaderBuffer = nullptr;
	HRESULT hr = _compileShaderFromFile(L"shader.fx", "VS", "vs_4_1", &vertexShaderBuffer);
	if (FAILED(hr)) { return false; }

	ID3DBlob *pixelShaderBuffer = nullptr;
	hr = _compileShaderFromFile(L"shader.fx", "PS", "ps_4_1", &pixelShaderBuffer);
	if (FAILED(hr)) { return false; }

	if (FAILED(hr)) { return false; }
	hr = _pDevice->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &_pVertexShader);
	if (FAILED(hr)) { return false; }

	hr = _pDevice->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &_pPixelShader);
	if (FAILED(hr)) { return false; }


	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	hr = _pDevice->CreateInputLayout(layout, numElements, vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), &_pLayout);
	if (FAILED(hr)) { return false; }

	_RELEASE(vertexShaderBuffer);
	_RELEASE(pixelShaderBuffer);

	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(samplerDesc));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	hr = _pDevice->CreateSamplerState(&samplerDesc, &_pSamplerState);
	if (FAILED(hr)) { return false; }

	return true;
}

void OBJ::CreateBuffers()
{
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	D3D11_SUBRESOURCE_DATA data;
	ZeroMemory(&data, sizeof(data));
	HRESULT hr = S_OK;

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(VERTEX)* _clearData.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	data.pSysMem = &_clearData[0];
	hr = _pDevice->CreateBuffer(&bd, &data, &_pVertexBuffer);
	if (FAILED(hr)) { return; }

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(DWORD)* _indices.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	data.pSysMem = &_indices[0];
	hr = _pDevice->CreateBuffer(&bd, &data, &_pIndexBuffer);
	if (FAILED(hr)) { return; }

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = _pDevice->CreateBuffer(&bd, NULL, &_pConstantBuffer);

	ClearMemory();
}

void OBJ::Render()
{
	_rot += 0.0005f;
	if (_rot > 6.26f) { _rot = 0.0f; }

	RenderBuffers();
	SetShaderParameters();
	RenderShader();

	for (int i = 0; i < _subsets.size(); i++)
	{
		_pImmediateContext->PSSetShaderResources(0, 1, &_textures[i]);
		int indexStart = _subsets[i]._startIndex;
		int indexEnd = ((i + 1) < _subsets.size()) ? _subsets[i + 1]._startIndex : _indices.size();
		int indexDrawAmount = indexEnd - indexStart;
		_pImmediateContext->DrawIndexed(indexDrawAmount, indexStart, 0);
	}
}


void OBJ::RenderBuffers()
{
	unsigned int stride = sizeof(VERTEX);
	unsigned int offset = 0;
	_pImmediateContext->IASetVertexBuffers(0, 1, &_pVertexBuffer, &stride, &offset);
	_pImmediateContext->IASetIndexBuffer(_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void OBJ::SetShaderParameters()
{
	XMVECTOR rotaxis = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMMATRIX rotation = XMMatrixRotationAxis(rotaxis, _rot);
	_world = rotation;

	XMMATRIX WVP = _world * _view * _projection;
	ConstantBuffer cb;
	cb.WVP = XMMatrixTranspose(WVP);
	_pImmediateContext->UpdateSubresource(_pConstantBuffer, 0, NULL, &cb, 0, 0);
	_pImmediateContext->VSSetConstantBuffers(0, 1, &_pConstantBuffer);

	_pImmediateContext->IASetInputLayout(_pLayout);
	_pImmediateContext->VSSetShader(_pVertexShader, NULL, 0);
	_pImmediateContext->PSSetShader(_pPixelShader, NULL, 0);
	_pImmediateContext->PSSetSamplers(0, 1, &_pSamplerState);
}


void OBJ::LoadTextures()
{
	ID3D11ShaderResourceView* tmpTexture;
	for (auto i = _subsets.begin(); i != _subsets.end(); i++)
	{
		HRESULT hr = CreateDDSTextureFromFile(_pDevice, ToString((*i)._material._ambientTextureMap), NULL, &tmpTexture);
		if (FAILED(hr))
		{
			_textures.push_back(nullptr);
		}
		else
		{
			_textures.push_back(tmpTexture);
		}
	}
}

wchar_t * OBJ::ToString(char* mbString)
{
	int len = 0;
	len = (int)strlen(mbString) + 1;
	wchar_t *ucString = new wchar_t[len];
	mbstowcs(ucString, mbString, len);
	return ucString;
}