#pragma once

// Directx includes
#include <d3d11.h>
#include <DirectXMath.h>
#include <d3dcompiler.h>
#include "DDSTextureLoader.h"

// c++ includes
#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <vector>

using namespace DirectX;
using namespace std;

#define MAX_FILENAME_LEN 50
#define _RELEASE(p)		{ if(p){(p)->Release(); (p)=nullptr;} }

struct ConstantBuffer
{
	XMMATRIX WVP;
};

struct VERTEX
{
	XMFLOAT3 _position;
	XMFLOAT3 _normal;
	XMFLOAT2 _texCoord;
};

struct MATERIAL
{
	XMFLOAT4 _ambientColor;
	XMFLOAT4 _diffuseColor;
	XMFLOAT4 _specularColor;
	float _specularPower;
	int _illuminationModel;

	char _ambientTextureMap[MAX_FILENAME_LEN];
	char _diffuseTextureMap[MAX_FILENAME_LEN];
	char _specularColorTextureMap[MAX_FILENAME_LEN];
	char _specularHighLightComponent[MAX_FILENAME_LEN];
	char _alphaTextureMap[MAX_FILENAME_LEN];
	char _bumpMap[MAX_FILENAME_LEN];
	char _displacementMap[MAX_FILENAME_LEN];
	char _stencilDecalTexture[MAX_FILENAME_LEN];

	MATERIAL():
		_ambientColor(1.0f, 1.0f, 1.0f, 1.0f),
		_diffuseColor(1.0f, 1.0f, 1.0f, 1.0f),
		_specularColor(0.0f, 0.0f, 0.0f, 0.0f),
		_specularPower(10.0f),
		_illuminationModel(1)
	{
		memset(_ambientTextureMap, 0, MAX_FILENAME_LEN);
		memset(_diffuseTextureMap, 0, MAX_FILENAME_LEN);
		memset(_specularColorTextureMap, 0, MAX_FILENAME_LEN);
		memset(_specularHighLightComponent, 0, MAX_FILENAME_LEN);
		memset(_alphaTextureMap, 0, MAX_FILENAME_LEN);
		memset(_bumpMap, 0, MAX_FILENAME_LEN);
		memset(_displacementMap, 0, MAX_FILENAME_LEN);
		memset(_stencilDecalTexture, 0, MAX_FILENAME_LEN);
	}
	friend ostream& operator<<(ostream& os, const MATERIAL& mtl);
};

struct SUBSET
{
	int _startIndex;
	MATERIAL _material;

	SUBSET(){ _startIndex = 0; }
	SUBSET(int startIndex){ _startIndex = startIndex; }
};

class OBJ
{
public:
	OBJ(ID3D11Device* pDevice, ID3D11DeviceContext* pImmediateContext, char *fileNameWithoutFormat);
	~OBJ();

	void LoadFromObjFileToMemory();
	void CreateBobjFromMemory();
	void CreateTXTFromMemory();
	void LoadFromBobjFileToMemory();

	void Init(XMMATRIX view, XMMATRIX projection);
	void Render();
private:
	void ClearMemory();
	MATERIAL GetMaterialFromFile(string mtlName);
	void LoadTextures();
	wchar_t * ToString(char* mbString);

	void CreateBuffers();
	bool InitShader();
	HRESULT _compileShaderFromFile(WCHAR* FileName, LPCSTR EntryPoint, LPCSTR ShaderModel, ID3DBlob** ppBlobOut);


	void RenderBuffers();
	void SetShaderParameters();
	void RenderShader();

	// ��� ���������
	ID3D11Device* _pDevice;
	ID3D11DeviceContext* _pImmediateContext;
	ID3D11Buffer* _pVertexBuffer;
	ID3D11Buffer* _pIndexBuffer;
	ID3D11Buffer* _pConstantBuffer;
	ID3D11VertexShader* _pVertexShader;
	ID3D11PixelShader* _pPixelShader;
	ID3D11InputLayout* _pLayout;
	ID3D11SamplerState* _pSamplerState;
	vector<ID3D11ShaderResourceView*> _textures;

	// �������
	XMMATRIX _world;
	XMMATRIX _view;
	XMMATRIX _projection;

	// ����� ������
	vector<XMFLOAT3> _vertices;
	vector<XMFLOAT3> _normals;
	vector<XMFLOAT2> _texCoords;

	// ������� �������
	vector<DWORD> _indices;
	vector<VERTEX> _clearData;
	vector<SUBSET> _subsets;

	string _objFileName;
	string _bobjFileName;
	string _mtlFileName;
	string _txtFileName;
	float _rot;
};

